﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllowTakeLetter : MonoBehaviour
{

    public char letter;
    GameGoalControler gameGoalControler;

    private void Start()
    {
        gameGoalControler = GameObject.FindGameObjectWithTag("Player").GetComponent<GameGoalControler>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            gameGoalControler.TakeLetter(letter);
            Destroy(gameObject);
        }
    }
}
