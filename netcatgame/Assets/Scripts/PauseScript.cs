﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScript : MonoBehaviour
{

    GmPanelsControler gmPanelsControler;
    public bool isPaused = false;

    private void Start()
    {
        gmPanelsControler = GameObject.FindGameObjectWithTag("GameController").GetComponent<GmPanelsControler>();
        gmPanelsControler.HideShowPanelPause("panelPauseH");
    }

    public void StopPouse()
    {
        isPaused = false;
        Time.timeScale = 1;
        gmPanelsControler.HideShowPanelPause("panelPauseH");
    }

    public void StartPouse()
    {
        isPaused = true;
        Time.timeScale = 0;
        gmPanelsControler.HideShowPanelPause("panelPause");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
            if (isPaused)
            {
                StopPouse();
            }
            else
                StartPouse();
    }
}
