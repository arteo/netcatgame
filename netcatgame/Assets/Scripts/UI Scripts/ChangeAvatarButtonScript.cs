﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeAvatarButtonScript : MonoBehaviour
{
    public Sprite[] avatars;
    public Image avatar;
    Image image;
    int indexImage = 1;

    void Start()
    {
        image = avatar.GetComponent<Image>();
    }

    public void ChangeImageLeft()
    {
        indexImage--;
        if (indexImage < 0)
        {
            indexImage = avatars.Length - 1;
        }
        setImage();
    }

    public void ChangeImageRight()
    {
        indexImage++;
        if (indexImage >= avatars.Length)
        {
            indexImage = 0;
        }
        setImage();
    }

    private void setImage()
    {
        image.sprite = avatars[indexImage];
    }

    public void SetIndexImage(int idx)
    {
        indexImage = idx;
    }
}
