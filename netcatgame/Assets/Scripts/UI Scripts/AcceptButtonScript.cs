﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Classes;
using UnityEngine;
using UnityEngine.UI;

public class AcceptButtonScript : MonoBehaviour
{
    public Image avatar;
    Image spriteAvatar;
    GmPlayerCharacterModeController gMPlayerCharacteristicModifer;
    PlayerClass playerSettings;

    private void Start()
    {
        gMPlayerCharacteristicModifer = GameObject.FindGameObjectWithTag("GameController").GetComponent<GmPlayerCharacterModeController>();
        spriteAvatar = avatar.GetComponent<Image>();
        playerSettings = gMPlayerCharacteristicModifer.GetSettings();
        DenyChange();
    }

    public Slider slider;
    public InputField inputField;

    public void AcceptChange()
    {
        gMPlayerCharacteristicModifer.ChangeSettings(spriteAvatar.sprite, inputField.text, slider.value);
    }

    public void DenyChange()
    {
        playerSettings = gMPlayerCharacteristicModifer.GetSettings();
        spriteAvatar.sprite = playerSettings.settings.avatar;
        slider.value = playerSettings.settings.audioValue;
        inputField.text = playerSettings.settings.namePlayer;
    }
}
