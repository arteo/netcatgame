﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsButtonScript : MonoBehaviour {

    GmPanelsControler gmPControl;
    // Use this for initialization
    void Start()
    {
        gmPControl = GameObject.FindGameObjectWithTag("GameController").GetComponent<GmPanelsControler>();
    }

    public void WatchSettings()
    {
        gmPControl.HideShowPanel("panelSettings");
    }
}
