﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderChangeVolumeMusicScript : MonoBehaviour
{
    AudioSource aus;
    Slider slider;

    // Use this for initialization
    void Start()
    {
        aus = GameObject.FindGameObjectWithTag("GameController").GetComponent<AudioSource>();
        slider = gameObject.GetComponent<Slider>();
    }


    public void ChangeVolume()
    {
        if (slider)
            aus.volume = slider.value;
    }
}
