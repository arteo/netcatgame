﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGameScript : MonoBehaviour
{

    public void CloseGame()
    {
        Application.Quit();
    }

}
