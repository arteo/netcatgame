﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewGameButtonScript : MonoBehaviour
{
    GmLoadSceneController gmLControl;
    // Use this for initialization
    void Start()
    {
        gmLControl = GameObject.FindGameObjectWithTag("GameController").GetComponent<GmLoadSceneController>();
    }

    public void NewGameStart()
    {
        gmLControl.LoadNewGameScene();
    }
}
