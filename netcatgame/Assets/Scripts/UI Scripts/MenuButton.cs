﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButton : MonoBehaviour
{
    public bool isGame = false;
    GmPanelsControler gmPControl;
    GmLoadSceneController gmLConstrol;

    private void Start()
    {
        gmPControl = GameObject.FindGameObjectWithTag("GameController").GetComponent<GmPanelsControler>();
        gmLConstrol = GameObject.FindGameObjectWithTag("GameController").GetComponent<GmLoadSceneController>();
        gmPControl.HideShowPanel("panelMenu");
    }

    public void GoToMenu()
    {
        if (isGame)
        {
            gmLConstrol.LoadMenuScene();
        }
        else
        {
            gmPControl.HideShowPanel("panelMenu");
        }
    }
}
