﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Classes;
using UnityEngine;
using UnityEngine.UI;

public class StatisticShow : MonoBehaviour
{
    public Image avatarImage;
    public Text timeText;
    public Text namePlayerText;
    Image avatarSprite;
    GmPlayerCharacterModeController gmPlayer;
    PlayerClass playerInfo;
    // Use this for initialization
    void Start()
    {
        gmPlayer = GameObject.FindGameObjectWithTag("GameController").GetComponent<GmPlayerCharacterModeController>();
        avatarSprite = avatarImage.GetComponent<Image>();
        playerInfo = gmPlayer.GetSettings();
    }

    void Update()
    {       
        avatarSprite.sprite=playerInfo.settings.avatar;
        timeText.text = playerInfo.GetTime();
        namePlayerText.text = playerInfo.settings.namePlayer;
    }
}
