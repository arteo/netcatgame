﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Classes
{
    public class PlayerClass
    {
        public SettingsClass settings;
        string time { get; set; }
        public PlayerClass()
        {
            settings = new SettingsClass();
            time = "00:00:00";
        }

        public void SetTime(DateTime timeDateTime)
        {
            time = "" + timeDateTime.Hour + ":" + timeDateTime.Minute + ":" + timeDateTime.Second;
        }

        public void SetTime(int sek, int? min=0, int? hour=0)
        {
            if (hour < 10) time = "0" + hour;
            else time = "" + hour;
            if (min < 10) time += ":0" + min;
            else time += ":" + min;
            if (sek < 10) time += ":0" + sek;
            else time += ":" + sek;
        }

        public void SetTime(string timeString = "00:00:00")
        {
            time = timeString;
        }

        public string GetTime()
        {
            return time;
        }

        public void SetSttings(SettingsClass set)
        {
            settings = set;
        }

        public SettingsClass GetSettings()
        {
            return settings;
        }
    }
}
