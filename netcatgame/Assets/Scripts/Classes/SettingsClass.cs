﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Classes
{
    public class SettingsClass : MonoBehaviour
    {
        public float audioValue;
        public Sprite avatar;
        public string namePlayer;

        public SettingsClass()
        {
            audioValue = .5f;
            namePlayer = "Player";
        }
    }
}
