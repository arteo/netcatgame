﻿using UnityEngine;

public class DontDestroyObjectOnLoadScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(this);
	}
}
