﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    public GameObject player;
    public Vector2 borderCameraMovent = new Vector2();

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        if (player.transform.position.x > borderCameraMovent.x && player.transform.position.x < borderCameraMovent.y)
        {
            transform.position = new Vector3(player.transform.position.x, 0, -10);
        }
        else if (player.transform.position.x < borderCameraMovent.x)
        {
            transform.position = new Vector3(borderCameraMovent.x, 0, -10);
        }
        else
        {
            transform.position = new Vector3(borderCameraMovent.y, 0, -10);
        }
    }
}
