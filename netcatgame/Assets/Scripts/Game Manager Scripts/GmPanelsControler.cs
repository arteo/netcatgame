﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GmPanelsControler : MonoBehaviour
{
    GameObject Menu;
    GameObject menuPanel;
    GameObject statisticsPanel;
    GameObject settingsPanel;
    GameObject pausePanel;

    public void HideShowPanel(string panelTag)
    {
        switch (panelTag)
        {
            case "panelMenu":
                HideShowPanelMenu();
                break;
            case "panelStatistics":
                HideShowPanelStatistics();
                break;
            case "panelSettings":
                HideShowPanelSettings();
                break;
        }
    }

    public void HideShowPanelMenu()
    {
        FindPanelsMenu();
        if ((menuPanel || statisticsPanel || settingsPanel))
        {
            menuPanel.SetActive(true);
            statisticsPanel.SetActive(false);
            settingsPanel.SetActive(false);
        }
    }

    public void HideShowPanelStatistics()
    {
        FindPanelsMenu();
        if ((menuPanel || statisticsPanel || settingsPanel))
        {
            menuPanel.SetActive(false);
            statisticsPanel.SetActive(true);
            settingsPanel.SetActive(false);
        }
    }

    public void HideShowPanelSettings()
    {
        FindPanelsMenu();
        if ((menuPanel || statisticsPanel || settingsPanel))
        {
            menuPanel.SetActive(false);
            statisticsPanel.SetActive(false);
            settingsPanel.SetActive(true);
        }
    }

    public void HideShowPanelPause(string panelTag)
    {
        switch (panelTag)
        {
            case "panelPauseH":
                if (!pausePanel) FindPanelsPause();
                pausePanel.SetActive(false);
                break;
            case "panelPause":
                if (!pausePanel) FindPanelsPause();
                pausePanel.SetActive(true);
                break;
        }
    }

    private void FindPanelsMenu()
    {
        if (!(menuPanel || statisticsPanel || settingsPanel))
        {
            menuPanel = GameObject.FindGameObjectWithTag("panelMenu");
            statisticsPanel = GameObject.FindGameObjectWithTag("panelStatistics");
            settingsPanel = GameObject.FindGameObjectWithTag("panelSettings");
        }
    }

    private void FindPanelsPause()
    {
        pausePanel = GameObject.FindGameObjectWithTag("panelPause");
    }
}
