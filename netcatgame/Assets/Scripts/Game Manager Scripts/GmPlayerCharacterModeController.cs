﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Classes;
using UnityEngine;

public class GmPlayerCharacterModeController : MonoBehaviour
{

    private PlayerClass playerSettings;
    public Sprite defaultAvatar;

    private void Start()
    {
        playerSettings = new PlayerClass();
        playerSettings.settings.avatar = defaultAvatar;
    }

    public void ChangeSettings(Sprite avatar, string name, float audioVolume)
    {
        if (playerSettings == null)
        {
            playerSettings = new PlayerClass();
            playerSettings.settings = new SettingsClass();
        }
        playerSettings.settings.avatar = avatar;
        playerSettings.settings.audioValue = audioVolume;
        playerSettings.settings.namePlayer = name;
    }

    public void ChangeAll(PlayerClass playerData)
    {
        playerSettings = playerData;
    }

    public PlayerClass GetSettings()
    {
        return playerSettings;
    }
}
