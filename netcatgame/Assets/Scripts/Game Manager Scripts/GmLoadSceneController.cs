﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GmLoadSceneController : MonoBehaviour
{
   
    GmPanelsControler gmPControl;
    // Use this for initialization
    void Start()
    {
        LoadMenuScene();
    }

    public void LoadMenuScene()
    {
        SceneManager.LoadSceneAsync(1);
    }

    public void LoadNewGameScene()
    {
        SceneManager.LoadSceneAsync(2);
    }
}

