﻿using System.Collections;
using Assets.Scripts.Classes;
using UnityEngine;
using UnityEngine.UI;

public class GameGoalControler : MonoBehaviour
{
    public Text timeText;
    public Text goalTextVisualization;
    public Camera kamera;
    public string word;

    GmLoadSceneController gmLoadingSceneController;
    GmPlayerCharacterModeController gmPlayerCharacteristicModifer;
    PauseScript pause;
    AudioSource audioSource;
    PlayerClass playerClass;

    char[] letterCollected;
    int collectLetter = 0;
    // Use this for initialization
    void Start()
    {
        gmPlayerCharacteristicModifer = GameObject.FindGameObjectWithTag("GameController").GetComponent<GmPlayerCharacterModeController>();
        pause = kamera.GetComponent<PauseScript>();
        gmLoadingSceneController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GmLoadSceneController>();
        audioSource = gameObject.GetComponent<AudioSource>();

        playerClass = gmPlayerCharacteristicModifer.GetSettings();
        audioSource.volume = playerClass.settings.audioValue;

        letterCollected = new char[word.Length];
        char[] oneChar = word.ToCharArray();

        for (int i = 0; i < word.Length; i++)
        {
            if (oneChar[i] == ' ')
                word.Remove(i, 1);
        }
        StartCoroutine(CkeckAnswear());
        StartCoroutine(TimeMeasure());
    }

    IEnumerator TimeMeasure()
    {
        int sek = 0, min = 0, hour = 0;
        while (true)
        {
            if (pause.isPaused == false)
            {
                sek++;
            }

            min = sek / 60;
            hour = min / 60;

            playerClass.SetTime(sek % 60, min % 60, hour);

            yield return new WaitForSecondsRealtime(1f);
        }
    }

    void Update()
    {
        timeText.text = playerClass.GetTime();
        if (Input.GetKeyDown(KeyCode.Space))
            audioSource.Play();
    }

    public void TakeLetter(char letter)
    {
        char[] c = word.ToCharArray();

        for (int i = 0; i < word.Length; i++)
        {
            if (c[i] == letter)
            {
                word.Remove(i, 1);
                letterCollected[i] = letter;
                collectLetter++;
                return;
            }
        }
    }

    IEnumerator CkeckAnswear()
    {
        while (word.Length > collectLetter)
        {
            goalTextVisualization.text = "";
            for (int i = 0; i < letterCollected.Length; i++)
            {
                goalTextVisualization.text += letterCollected[i];
            }

            yield return new WaitForSecondsRealtime(.2f);
        }

        gmPlayerCharacteristicModifer.ChangeAll(playerClass);
        gmLoadingSceneController.LoadMenuScene();
    }
}
