﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Classes;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    Animator animator;
    SpriteRenderer spriteRenderer;
    AudioSource audioSource;
    Rigidbody2D rigidbody2d;

    [Range(1, 7)]
    public int hp = 3;
    public float speed, forceJump;
    public AudioClip jumpSound;

    private bool isAlive = true;
    bool isGrounded = true;  

    private void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
        rigidbody2d = GetComponent<Rigidbody2D>();
        animator = gameObject.GetComponent<Animator>();
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();

        audioSource.clip = jumpSound;
    }

    void Update()
    {
        if (isAlive)
        {
            Turn();
            ChangeSpeed();
            Jump();
            AnimationControl();
        }
    }

    void Turn()
    {
        if (Input.GetAxis("Horizontal") < 0 && spriteRenderer.flipX == false)
        {
            spriteRenderer.flipX = true;
        }
        else if (Input.GetAxis("Horizontal") > 0 && spriteRenderer.flipX == true)
        {
            spriteRenderer.flipX = false;
        }
    }

    void ChangeSpeed()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            rigidbody2d.velocity = new Vector2(speed * 2f * Input.GetAxis("Horizontal"), rigidbody2d.velocity.y);
        }
        else
        {
            rigidbody2d.velocity = new Vector2(speed * Input.GetAxis("Horizontal"), rigidbody2d.velocity.y);
        }
    }

    void Jump()
    {
        if (isGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            rigidbody2d.AddForce(new Vector2(0, forceJump), ForceMode2D.Impulse);
            audioSource.Play();
        }
    }

    void AnimationControl()
    {
        if (rigidbody2d.velocity.y == 0)
            AnimationControlInGround();
        else
            AnimationControlOnAir();
    }

    void AnimationControlInGround()
    {
        if (rigidbody2d.velocity.x == 0)
        {
            animator.Play("Idle");
        }
        else if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            animator.Play("Run");
        }
        else
        {
            animator.Play("Walk");
        }
    }

    void AnimationControlOnAir()
    {
        if (rigidbody2d.velocity.y > 0)
            animator.Play("Jump");
        else if (rigidbody2d.velocity.y < 0)
            animator.Play("Fall");
    }

    void TakeDamage()
    {
        hp--;
        if (hp > 0) animator.Play("Hurt");
        else
        {
            isAlive = false;
            animator.Play("Dead");
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        isGrounded = true;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        isGrounded = false;
    }
}
